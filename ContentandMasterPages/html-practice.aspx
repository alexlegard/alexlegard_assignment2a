﻿<%@ Page Title="HTML practice" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="asp-practice.aspx.cs" Inherits="ContentandMasterPages.asp_practice" %>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server">
        <h3>A tricky concept</h3>
        <p>Setting a box to be an inline block.</p>
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
        <h3>Example code</h3>
        <p>.page-wrapper {
	    /*border: 1px solid black;*/
	    max-width:1200px;
	    max-height:3000px;
	    margin:0 auto;
	    background-color:#ffcc00;}
        </p>
</asp:Content>


<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
        <h3>CSS from elsewhere to make a 3 column layout</h3>
        <p>
            /* Create three equal columns that floats next to each other */
            .column {
            float: left;
            width: 33.33%;
            padding: 10px;
            height: 300px;} /* Should be removed. Only for demonstration */

            /* Clear floats after the columns */
            .row:after {
            content: "";
            display: table;
            clear: both;}
        </p>
</asp:Content>


<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
        <h3>Helpful resources</h3>
        <ul>
            <li>Stack Exchange</li>
            <li>W3Schools HTML tutorials</li>
            <li>W3Schools CSS tutorials</li>
        </ul>
</asp:Content>