﻿<%@ Page Title="ASP Practice" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="asp-practice.aspx.cs" Inherits="ContentandMasterPages.asp_practice" %>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server">
        <h3>A tricky concept</h3>
        <p>The ContentPlaceholderID must match the ID of the ContentPlaceHolder in the Site.Master file. You need a separate
            control in the master file for each content placeholder you want to put in each page. (Double references are not allowed)
        </p>
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
        <h3>Example code</h3>
        <p>Width: 3</p>
</asp:Content>


<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
        <h3>Some code found elsewhere</h3>
        <p>Width: 4</p>
</asp:Content>


<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
        <h3>Helpful resources</h3>
        <ul>
            <li>W3Schools</li>
            <li>Microsoft resources</li>
            <li>Stack Exchange</li>
        </ul>
</asp:Content>