﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ContentandMasterPages._Default" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="jumbotron" runat="server">   
    <div>
        <h1>HTTP STUDY GUIDE</h1>
        <p class="lead">With this powerful study guide, my tests will be EASY</p>
    </div>
</asp:Content>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server">
    <h3>Javascript exam (completed)</h3>
    <p>Monday, Oct. 15</p>
    <p>Alt exam centre</p>
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
    <h3>HTML exam</h3>
    <p>Alt exam centre</p>
</asp:Content>

<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
    <h3>ASP exam</h3>
    <p>In class</p>
</asp:Content>

<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
    <h3>SQL exam</h3>
    <p>Alt exam centre</p>
</asp:Content>