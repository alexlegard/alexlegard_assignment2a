﻿<%@ Page Title="Javascript practice" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="asp-practice.aspx.cs" Inherits="ContentandMasterPages.asp_practice" %>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server">
        <h3>A tricky concept</h3>
        <p>The new keyword is only used when instantiating a new object. It is not used when making arrays. For arrays, you use
            this syntax:

            var animals = [grasshopper, dragon, penguin, unicorn];
        </p>
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
        <h3>Example of an if statement</h3>
        <p>if(firstName == "Sean"){
	    alert("You are Sean");
        } else if(firstName == "Bernie") {
	    alert("You are Bernie");
        } else {
	    alert("I don't know your name.")
        }
        </p>
</asp:Content>


<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
        <h3>Some code found elsewhere</h3>
        <p>
            // Create an object:
            var car = {type:"Fiat", model:"500", color:"white"};

            // Display some data from the object:
            document.getElementById("demo").innerHTML = "The car type is " + car.type;
        </p>
</asp:Content>


<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
        <h3>Helpful resources</h3>
        <ul>
            <li>W3Schools Javascript resources</li>
            <li>Stack Exchange</li>
            <li>AAAND Google</li>
        </ul>
</asp:Content>